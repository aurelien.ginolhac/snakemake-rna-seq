[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.10079215.svg)](https://doi.org/10.5281/zenodo.10079215)


## RNA-seq analysis with Snakemake

[Snakemake](https://snakemake.readthedocs.io/en/stable/) is a workflow manager system for
creating reproducible and scalable pipelines. See a < 2 min 
[video presentation](https://www.youtube.com/watch?v=UOKxta3061g) that scheme the main principles.

This repository allows you to perform a *RNA-seq analysis*, from raw reads to differential expression and is customised 
from [this template](https://github.com/snakemake-workflows/rna-seq-star-deseq2) by **Johannes Köster**.
Dependencies between steps are depicted below:

![](https://i.imgur.com/QYeG6Oi.png)


Reads can be supplied either as **single-end** or **paired-end** and optionally with technical replicates.
Sequencing files from technical replicates are merged at the mapping step.

### Tools used

- Reads are trimmed by [`AdapterRemoval`](https://github.com/MikkelSchubert/adapterremoval) 
and aligned by [`STAR`](https://github.com/alexdobin/STAR).
- References (genome sequence are gene annotation) fetching from Ensembl are included in the workflow
- Quality controls are performed by:
    + [`fastqc`](https://www.bioinformatics.babraham.ac.uk/projects/fastqc/) 
    + [`fastq Screen`](https://www.bioinformatics.babraham.ac.uk/projects/fastq_screen/)
    + [`multiqc`](https://multiqc.info/) will gather qc, trimming, alignment outputs and counting statistic for producing a final `html` report.
- gene counts are available by `STAR` for QC but more consistently (with mapping quality >= 30 and strandeness user-provided) by [`Rsubread::featureCounts`](https://bioconductor.org/packages/release/bioc/vignettes/Rsubread/inst/doc/SubreadUsersGuide.pdf)
- differential expression using the [`R`](https://cran.r-project.org/) package [`DESeq2`](http://bioconductor.org/packages/release/bioc/html/DESeq2.html)

Importantly, all software are available in a [docker image](https://hub.docker.com/repository/docker/ginolhac/snake-rna-seq). 
`Snakemake` will automatically fetch the image and convert it to a [Singularity](https://sylabs.io/) image and cache it until the tag change (or image is deleted from the `.snakemake/singularity/` folder.

### Install

On the HPC [iris](https://hpc.uni.lu/systems/iris/), what is needed is:

- `snakemake`
- `singularity`
- This workflow template


#### Use `snakemake` module

On HPC, we use `modules`, use that:

```
module use /work/projects/dlsm-soft/easybuild/modules/all/
module load tools/snakemake
```

#### Fetch the workflow template

```bash
VERSION="v0.5.4"
wget -qO- https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-rna-seq/-/archive/${VERSION}/snakemake-rna-seq-.tar.gz | tar xfz - --strip-components=1
```

This command will download, extract (without the root folder) the following files

```
CHANGELOG.md
config/
Dockerfile
LICENSE
README.md
workflow/
```

You may want to delete the `LICENSE`, `Dockerfile`, `CHANGELOG.md` and `README.md` if you wish, 
they are not used by `snakemake`

#### Fetch the test data

```bash
wget -qO- https://EvQWtxQ6xLehNEf:yeast@owncloud.lcsb.uni.lu/public.php/webdav/fq_yeast_testdata.tgz | tar xvfz -
```

You should obtain the following 5 FASTQ gzipped files in a `fastq` folder

```
├── fastq
│   ├── BY_A-2.fastq.gz
│   ├── BY_A.fastq.gz
│   ├── BY_B.fastq.gz
│   ├── BY_Gis1_A.fastq.gz
│   └── BY_Gis1_C.fastq.gz
```

The biological sample `BY_A` comes into 2 technical replicates. 

Finally, you file structure should look like

```
.
├── CHANGELOG.md
├── Dockerfile
├── config
│   ├── config.yaml
│   ├── samples.tsv
│   └── units.tsv
├── fastq
│   ├── BY_A-2.fastq.gz
│   ├── BY_A.fastq.gz
│   ├── BY_B.fastq.gz
│   ├── BY_Gis1_A.fastq.gz
│   └── BY_Gis1_C.fastq.gz
├── LICENSE
├── README.md
└── workflow
    ├── report
    │   ├── diffexp.rst
    │   ├── ma.rst
    │   ├── pca.rst
    │   ├── pc.rst
    │   ├── session.rst
    │   ├── volcano.rst
    │   └── workflow.rst
    ├── rules
    │   ├── align.smk
    │   ├── common.smk
    │   ├── diffexp.smk
    │   ├── qc.smk
    │   ├── refs.smk
    │   └── trim.smk
    ├── schemas
    │   ├── config.schema.yaml
    │   ├── samples.schema.yaml
    │   └── units.schema.yaml
    ├── scripts
    │   ├── count-matrix.py
    │   ├── deseq2-init.R
    │   ├── deseq2.R
    │   ├── featureCounts.R
    │   ├── gtf2bed.py
    │   ├── plot-pca.R
    │   └── session.R
    ├── slurm_profile
    │   ├── config.yaml
    │   └── iris.yaml
    └── Snakefile
```


#### Singularity

Like `snakemake`, available as a [module](https://hpc.uni.lu/users/software/)

Load it with:

``` bash
module load tools/Singularity
```

Check the tags of the Docker images on [Docker Hub](https://hub.docker.com/r/ginolhac/snake-rna-seq/tags).
The current tag used is mentioned in `workflow/Snakefile`. 

#### Reporting in publications

Both release tag for the template and for the Docker images need to be reported along with the URL to this page

#### Aliases

Since we need some setup each time, I have those aliases in my `.bashrc`

```bash
alias mls='module load tools/snakemake && module load tools/Singularity'
alias dag='snakemake --dag | dot -Tpdf > dag.pdf'
```

Ince on a `node`, I can quickly activate the right `conda` environment and load `Singularity`.
`dag` is useful with shortly get the pdf of the dependencies task. Of note, you can change the file format to `png` or `svg` if you prefer.
Lastly, the `complete` instruction is for the auto-completion in bash.


### Usage

Book resources on `aion`, like 12 cores for 1 hour with:

```bash
si -t 0-1:00:0 -N 1 -c 12 
```

Once on a `node` you can use the alias `mls` to activate the environment and load `Singularity` and `snakemake`

### Test yeast data

#### Task dependencies: [Directed Acyclic Graph (DAG)](https://en.wikipedia.org/wiki/Directed_acyclic_graph)


Now, we have one hour to run the workflow. By default, `snakemake` will find automatically the `Snakefile` present in `workflow/`.

First of all, compute the dependency graph:

`dag` the alias of `snakemake --dag | dot -Tpdf > dag.pdf`

you should obtain the following:

![](https://i.imgur.com/p4Tu61m.png)

All *plain* lines mean that the task is not complete, they will be *dashed* when achieved.

You can obtain a simplified DAG with `--rulegraph` instead of `--dag`.
Or a more complete DAG with filenames using `--filegraph`. This one is useful because it shows clearly the `input` and `output` of each rule.

See a partial picture of it below

![](https://i.imgur.com/1rJnEue.png)

As tabulated-text, the same output is obtained with `snakemake --summary` (here first 10 entries retabled for display purposes)

```
output_file     date    rule    version log-file(s)     status  plan
results/diffexp/KO_vs_WT.diffexp.tsv    -       deseq2  -       logs/deseq2/KO_vs_WT.diffexp.log        missing update pending
results/diffexp/KO_vs_WT.ma-plot.pdf    -       deseq2  -       logs/deseq2/KO_vs_WT.diffexp.log        missing update pending
results/diffexp/KO_vs_WT.plot-count.pdf -       deseq2  -       logs/deseq2/KO_vs_WT.diffexp.log        missing update pending
results/diffexp/KO_vs_WT.volcano.pdf    -       deseq2  -       logs/deseq2/KO_vs_WT.diffexp.log        missing update pending
results/pca.pdf                         -       pca     -       logs/pca.log                            missing update pending
qc/multiqc_report.html                  -       multiqc -       logs/multiqc.log                        missing update pending
qc/fastqc/WT_A-1.html                   -       fastqc  -       logs/fastqc/WT_A-1.log                  missing update pending
qc/fastqc/WT_A-1_fastqc.zip             -       fastqc  -       logs/fastqc/WT_A-1.log                  missing update pending
qc/fastqc/WT_A-2.html                   -       fastqc  -       logs/fastqc/WT_A-2.log                  missing update pending
[...]
```

Once some files are obtained, the missing files are filling up.

#### Running the workflow

A `dry-run` to start with is recommended:

```bash
snakemake --use-singularity --singularity-args "-B /work/projects/dlsm-soft/smk:/mnt/data/" --cores 12 -n
```

- the `--use-singularity` option indicates `snakemake` to fetch and use the docker image linked in the `Snakefile`.
- the `--singularity-args` bind  pre-indexed references folder to the singularity container for `fastq_screen`.
- we booked *12* cores, tell `snakemake` about it with `--cores`, finally `-n` is the `dry-run flag.

A long list of _green_ instructions should be printed out, and finally in _yellow_ the summary:

```
Job counts:
        count   jobs
        5       adapter_removal_se
        4       align_multi
        1       all
        1       deseq2
        1       deseq2_init
        5       fastq_screen
        5       fastqc
        1       feature_count
        1       get_annotation
        1       get_genome
        1       multiqc
        1       pca
        1       session
        1       star_index
        29
This was a dry-run (flag -n). The order of jobs does not reflect the order of execution.
```

**29 tasks** need to be run. Now run the same command, *without* the `-n` flag.

If all goes well (with 12 cores, it should take ~ 10 min), which is: no red text appears, you can generate the final report using:

```bash
snakemake --report
```

#### CeL-ID workflow

A methology published by [Mohammad _et al._ 2019](https://bmcgenomics.biomedcentral.com/track/pdf/10.1186/s12864-018-5371-9.pdf) allows from 
RNA-seq sequences to identify similarities between your samples and 934 known human cell lines.

Activate the option in `config/config.yaml`:

``` yaml
celid: true
```

**Disclaimer**

The liftOver approach works well for this identification but variants should **not** be used for
 cancer research, _i. e_ functional snps analysis. As stated [here](https://genome.ucsc.edu/FAQ/FAQreleases.html#snpConversion):

>While the LiftOver tool exists to convert coordinates between assemblies, it is NOT recommended to use LiftOver to convert SNPs between two assembly versions.

Note that this only works if the reference genome is `GRCh38` as the obtained variants are [lift over](https://gatk.broadinstitute.org/hc/en-us/articles/360050815392-LiftoverVcf-Picard-) to `GRCh37` since [their methodology](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7643850/pdf/BioProtoc-10-03-3507.pdf) used this assembly.

### Expected outputs

- The `snakemake` report: `report.html`, download a [copy here](https://EvQWtxQ6xLehNEf:yeast@owncloud.lcsb.uni.lu/public.php/webdav/report.html)
- The `multiqc` report: `qc/multiqc_report.html`, download a [copy here](https://EvQWtxQ6xLehNEf:yeast@owncloud.lcsb.uni.lu/public.php/webdav/multiqc_report.html)

in the report, the different versions of used are in `session.txt` and contains:

```
STAR 2.7.11a
samtools 1.18
FastQC v0.12.1
FastQ Screen v0.15.3
AdapterRemoval ver. 2.3.3

R version 4.3.0 (2023-04-21)
Ubuntu 22.04.1 LTS

package version date    source
ggplot2 3.4.3   2023-08-14      RSPM (R 4.2.0)
DESeq2  1.40.2  2023-06-23      Bioconductor
apeglm  1.22.0  2023-04-25      Bioconductor
Rsubread        2.8.1   2021-11-07      Bioconductor
```

The knock-out of _GIS1_ worked out well and the plot of normalized count is a good control for the directionality.
The Gene counts from STAR are kept because in the `multiqc_report` one can check the library orientation 
here clearly *reversed-stranded*.
Of note, the mapping rate is artificially 100% because I selected those reads _a posteriori_.

#### For CeL-ID

For each sample, here for example using the public run of a **MelJuSo** cell line [SRR9159640](https://www.ebi.ac.uk/ena/browser/view/SRR9159640) (only the read 1), the obtained TSV looks like:

```
rank    cell_lines      nb_snps rho     p_value
top1    G28031.MEL-JUSO.1       370     0.9361216756223799      1.7472961810370592e-23
top2    G30645.SW48.3   96      0.5361481126606673      0.07965341986358575
top3    G27299.BICR_56.1        191     0.5330345888822653      0.08993401725152764
top4    G27470.Pfeiffer.2       208     0.5017159286372762      0.2498991986837091
top5    G30628.ST486.1  124     0.49400711305791        0.3047491823009211
bottom5 G27281.RERF-LC-MS.1     212     -0.07153177284517191    1
bottom4 G41736.T.T.5    215     -0.07856528394856224    1
bottom3 G27367.BFTC-909.1       190     -0.09700121723660658    1
bottom2 G30575.TUHR10TKB.1      179     -0.11462356452349458    1
bottom1 G26208.JHOC-5.2 199     -0.13786754154389713    1
```

### Writing units.tsv files

The supplied template describes a single-end experiment: the field `fq2` is left empty.
Moreover, one sample has 2 technical replicates: described as unit: `1` and `2`.
So, after alignment of tabulated fields, it looks like this:

```
sample  unit    fq1                     fq2     strandedness
WT_A    1       fastq/BY_A.fastq.gz             reverse
WT_A    2       fastq/BY_A-2.fastq.gz           reverse
```

If you have **paired-end** reads, fill out the `fq2` field with the path for `R2` file.
See example below, where:

- WT_A is paired-end and with 2 technical replicates
- WT_B is paired-end without technical replicates
- KO_A is single-end and with 2 technical replicates
- WT_A is single-end without technical replicates

```
sample  unit  fq1                           fq2                      strandedness
WT_A    1     fastq/BY_A_R1-1.fastq.gz      fastq/BY_A_R2-1.fastq.gz reverse
WT_A    2     fastq/BY_A_R1-2.fastq.gz      fastq/BY_A_R2-2.fastq.gz reverse
WT_B    1     fastq/BY_B_R1.fastq.gz        fastq/BY_B_R2.fastq.gz   reverse
KO_A    1     fastq/BY_Gis1_A-1.fastq.gz                             reverse
KO_A    2     fastq/BY_Gis1_A-2.fastq.gz                             reverse
KO_C    1     fastq/BY_Gis1_C.fastq.gz                               reverse
```

The DAG of such `unit.tsv` will looks like: 

![](https://i.imgur.com/xr5H66L.png)

### Cleaning all output

Cleaning output files, here with 2 jobs can be performed with:

```bash
snakemake -c 2 --delete-all-output
```

Of note, this does not remove the folders, only the files.

### Helpers

To be adapted to your fastq filenames.

For `units.tsv`

```
ls fastq/*z | paste - - | awk 'BEGIN{OFS="\t";print "sample\tunit\tfq1\tfq2\tstrandedness"}{id=substr($1,index($1,"/")+1,index($1,"_")-7);print id,"1",$1,$2, "reverse"}'
```

`samples.tsv`

```
awk 'BEGIN{OFS="\t";print "sample\tcondition"}{if(NR>1){id=substr($1,1,index($1,"_")-1); print $1, id}}' config/units.tsv
```

### Slurm launcher script

As an example, to be adapted

```
#!/bin/bash -l
#SBATCH -N 1
#SBATCH -J XX
#SBATCH --mail-type=begin,end,fail
#SBATCH --mail-user=xx@xx.x
#SBATCH --ntasks-per-node=1
#SBATCH --mem=40GB
#SBATCH -c 24
#SBATCH --time=0-07:30:00
#SBATCH -p batch

module load tools/Singularity
module load tools/snakemake

snakemake --use-singularity --singularity-args "-B /work/projects/dlsm-soft/smk:/mnt/data" --cores "${SLURM_CPUS_PER_TASK}"

```


### Limitations

- Paired-end and single-end can be mixed in one experiment, but a same biological sample must not be a mix of both. In other words, technical replicates from one sample must be either single-end **or** paired-end.
- The strandeness needs to be described for every fastq. Might be a global flag since
since it is unlikely to mix different strandeness in one analysis.
- `deseq2` object is created removing by default genes with global count < 10 as recommended. Could be in the config.
- `deseq2` shrinkage is done with `apeglm` since it will be the default. Could also be an option in config.
- References, both `fasta` and `gtf` should be shared between people using [`SNAKEMAKE_OUTPUT_CACHE`](https://snakemake.readthedocs.io/en/stable/executing/caching.html).
- The [`sortMeRNA`](https://github.com/biocore/sortmerna) tool is not included, we had too many issues with it 
and the QC steps can now monitor if rRNA are present.
- Inspired by the [nextflow template](https://github.com/nf-core/rnaseq/blob/master/docs/output.md), `RSeQC` *read distribution* would be a nice QC to have


### Debugging

Clear blogpost by Vince Buffalo [Understanding Snakemake](https://vincebuffalo.com/blog/2020/03/04/understanding-snakemake.html)

`expand()` work with the wildcards from the `output` of **rules**.

- Preamble, load tools and config files (and validate them)

```python
import pandas as pd
import numpy as np
import snakemake
from snakemake.io import expand

config = snakemake.io.load_configfile("config/config.yaml")
snakemake.utils.validate(config, schema="workflow/schemas/config.schema.yaml")

samples = pd.read_table(config["samples"]).set_index("sample", drop=False)
snakemake.utils.validate(samples, schema="workflow/schemas/samples.schema.yaml")

units = pd.read_table(config["units"], dtype=str).set_index(["sample", "unit"], drop=False)
units.index = units.index.set_levels([i.astype(str) for i in units.index.levels])  # enforce str in index
snakemake.utils.validate(units, schema="workflow/schemas/units.schema.yaml")
# load smk functions https://stackoverflow.com/a/16604453/1395352
#exec(compile(source=open('common.smk').read(), filename='common.smk', mode='exec'))

```

- First tests

```python
html="qc/fastqc/{sample}-{unit}.html"
expand(html, sample=units["sample"].tolist(), unit=units["unit"].tolist())
['qc/fastqc/WT_A-1.html', 'qc/fastqc/WT_A-1.html', 'qc/fastqc/WT_A-1.html', 'qc/fastqc/WT_A-1.html', 'qc/fastqc/WT_B-1.html', 'qc/fastqc/WT_B-1.html', 'qc/fastqc/WT_B-1.html', 'qc/fastqc/WT_B-1.html', 'qc/fastqc/KO_A-1.html', 'qc/fastqc/KO_A-1.html', 'qc/fastqc/KO_A-1.html', 'qc/fastqc/KO_A-1.html', 'qc/fastqc/KO_C-1.html', 'qc/fastqc/KO_C-1.html', 'qc/fastqc/KO_C-1.html', 'qc/fastqc/KO_C-1.html']
# see also the solution using panda itertuples
expand("qc/fastqc/{unit.sample}-{unit.unit}_fastqc.zip", unit=units.itertuples())
(['qc/fastqc/WT_A-1_fastqc.zip', 'qc/fastqc/WT_B-1_fastqc.zip', 'qc/fastqc/KO_A-1_fastqc.zip', 'qc/fastqc/KO_C-1_fastqc.zip'],)
```

For units, like

```python
# simple example, return desired row with .loc
# units is indexed by both sample and unit
units.index
MultiIndex([('WT_A', '1'),
            ('WT_B', '1'),
            ('KO_A', '1'),
            ('KO_C', '1')],
           names=['sample', 'unit'])

units.loc[("WT_A", "1"), ["fq1", "fq2"]]
# for single-end, discard the NA due to missing fq2
units.loc[("WT_A", "1"), ["fq1", "fq2"]].dropna()

# function are actually called for every unit as in multiqc rule
[get_settings(unit.sample, unit.unit) for unit in units.itertuples()]
[['trimmed_pe/WT_A-1.settings'], ['trimmed_pe/WT_A-2.settings'], ['trimmed_pe/WT_B-1.settings'], 'trimmed_se/KO_A-1.settings', 'trimmed_se/KO_A-2.settings', 'trimmed_se/KO_C-1.settings']

# for fastq paired-end
[get_fastq(unit) for unit in units.itertuples()]
[fq1    fastq/BY_A.fastq_1.gz
fq2    fastq/BY_A.fastq_2.gz
Name: (WT_A, 1), dtype: object, fq1    fastq/BY_A.fastq_1.2.gz
fq2    fastq/BY_A.fastq_2.2.gz
Name: (WT_A, 2), dtype: object, fq1    fastq/BY_B.fastq_1.gz
fq2    fastq/BY_B.fastq_2.gz
Name: (WT_B, 1), dtype: object, fq1    fastq/BY_Gis1_A.fastq_1.gz
Name: (KO_A, 1), dtype: object, fq1    fastq/BY_Gis1_A.fastq_1.2.gz
Name: (KO_A, 2), dtype: object, fq1    fastq/BY_Gis1_C.fastq_1.gz
Name: (KO_C, 1), dtype: object]


[is_single_end(unit.sample, unit.unit) for unit in units.itertuples()]
# new version with sample only
[is_single_end(sample.sample) for sample in samples.itertuples()]

# values to extract the dataframe, then to python list after flattening
[units.loc[(unit.sample), ["fq1"]].values.flatten().tolist() for unit in samples.itertuples()]
[get_fq1(sample.sample)  for sample in samples.itertuples()]


```
