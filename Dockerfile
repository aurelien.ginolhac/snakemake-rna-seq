FROM eddelbuettel/r2u:jammy

LABEL org.opencontainers.image.authors="Aurelien Ginolhac <aurelien.ginolhac@uni.lu>"

# build with `docker buildx build -t ginolhac/snake-rna-seq:0.8 .`
# docker push ginolhac/snake-rna-seq:0.8

# cel-id added in tag 0.7
LABEL version="0.8"  
LABEL description="Docker image to build the snakemake rna-seq singularity"

ARG DEBIAN_FRONTEND=noninteractive

# add-apt-repository -y ppa:openjdk-r/ppa && \
RUN apt-get update && apt-get install -y software-properties-common && \
    apt-get update && apt-get install -y \
        build-essential \
        cmake \
        curl \
        gpg-agent \
        libboost-all-dev \
        libbz2-dev \
        libcurl4-openssl-dev \
        liblzma-dev \
        libncurses5-dev \
        libssl-dev \
        libxml2-dev \
        libgd-perl libgd-graph-perl \
        openjdk-8-jdk \
        python-is-python3 \
        python-dev-is-python3 \
        python3-pip \
        unzip \
        vim-common \
        wget \
        zlib1g-dev

# default to python3 
# better to use https://stackoverflow.com/a/50331137/1395352
RUN ln -sf /usr/bin/python-config /usr/bin/python3.10-config && ln -sf /usr/bin/python3 /usr/bin/python

# htslib
ARG HTS_VERSION="1.18"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/samtools/htslib/releases/download/${HTS_VERSION}/htslib-${HTS_VERSION}.tar.bz2 && \
    tar -xf htslib-${HTS_VERSION}.tar.bz2 && rm htslib-${HTS_VERSION}.tar.bz2 && cd htslib-${HTS_VERSION} && \
    ./configure --enable-libcurl --enable-s3 --enable-plugins --enable-gcs && \
    make && make install && make clean


# samtools
ARG SAM_VERSION="1.18"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/samtools/samtools/releases/download/${SAM_VERSION}/samtools-${SAM_VERSION}.tar.bz2 && \
    tar -xf samtools-${SAM_VERSION}.tar.bz2 && rm samtools-${SAM_VERSION}.tar.bz2 && cd samtools-${SAM_VERSION} && \
    ./configure --with-htslib=/opt/htslib-${HTS_VERSION} && make && make install && make clean

# Picard tools
ARG PIC_VERSION="2.27.4"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/broadinstitute/picard/releases/download/${PIC_VERSION}/picard.jar && \
    wget --no-check-certificate -q -O /usr/local/bin/picard https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-chip-seq/-/raw/main/workflow/scripts/picard && \
    chmod +x /usr/local/bin/picard


ARG STAR_VERSION="2.7.11a"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/alexdobin/STAR/archive/${STAR_VERSION}.tar.gz && \
    tar -xf ${STAR_VERSION}.tar.gz && rm ${STAR_VERSION}.tar.gz && \
    make STAR -C STAR-${STAR_VERSION}/source && make STARlong -C STAR-${STAR_VERSION}/source && \
    mv STAR-${STAR_VERSION}/source/STAR* STAR-${STAR_VERSION}/bin/Linux_x86_64/
ENV PATH /opt/STAR-${STAR_VERSION}/bin/Linux_x86_64:$PATH
# ln -s /opt/STAR-${STAR_VERSION}/bin/Linux_x86_64_static/STAR* /usr/local/bin/

# bedtools
ARG BT_VERSION="2.31.0"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/arq5x/bedtools2/releases/download/v${BT_VERSION}/bedtools-${BT_VERSION}.tar.gz && \
    tar -xf bedtools-${BT_VERSION}.tar.gz && rm bedtools-${BT_VERSION}.tar.gz && \
    cd bedtools2 && make && make install && make clean

# AdapterRemoval
ARG AR_VERSION="2.3.3"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/MikkelSchubert/adapterremoval/archive/v${AR_VERSION}.tar.gz && \
    tar -xf v${AR_VERSION}.tar.gz && rm v${AR_VERSION}.tar.gz && \
    cd adapterremoval-${AR_VERSION} && make && make install && make clean

# Bowtie2
ARG BT2_VERSION="2.5.1"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/BenLangmead/bowtie2/releases/download/v${BT2_VERSION}/bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    unzip bowtie2-${BT2_VERSION}-linux-x86_64.zip && rm bowtie2-${BT2_VERSION}-linux-x86_64.zip && \
    mv bowtie2-${BT2_VERSION}-linux-x86_64/bowtie2* /usr/local/bin/ && rm -rf bowtie2-${BT2_VERSION}-linux-x86_64

# FastScreen
ENV PERL_MM_USE_DEFAULT=1
ARG FQ_SCREEN_VERSION="0.15.3"
RUN cd /opt && \
    wget --no-check-certificate -q https://github.com/StevenWingett/FastQ-Screen/archive/refs/tags/v${FQ_SCREEN_VERSION}.tar.gz && \
    tar -xf v${FQ_SCREEN_VERSION}.tar.gz && rm v${FQ_SCREEN_VERSION}.tar.gz
ENV PATH /opt/FastQ-Screen-${FQ_SCREEN_VERSION}:$PATH

# FastQC
ENV FQC_VERSION="0.12.1"
RUN cd /opt && \
    wget --no-check-certificate -q https://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v${FQC_VERSION}.zip && \
    unzip fastqc_v${FQC_VERSION}.zip && rm fastqc_v*.zip && chmod +x FastQC/fastqc
ENV PATH /opt/FastQC:$PATH

# python modules
RUN python3 -m pip install snakemake snakemake-wrapper-utils multiqc pandas

# R is 4.3.2
# DESeq2 is 1.42.0
# Rsubread is 2.16.0
# apelglm 1.24.0 
RUN install.r  ggplot2 hexbin ggrepel cowplot stringr \
  deseq2 apeglm rsubread  fst dplyr readr ggplot2 tidyr \
  backports broom circlize ComplexHeatmap DelayedArray RcppEigen emdbook RcppNumerical && \
  installGithub.r koncina/ek.plot

  
  
# varscan2
ARG VS2_VERSION="2.4.6"
RUN cd /opt && \
   wget --no-check-certificate -q -O varscan.jar https://github.com/dkoboldt/varscan/releases/download/v${VS2_VERSION}/VarScan.v${VS2_VERSION}.jar 

# For CeL-ID Perl SWitch.pm
RUN apt-get install -y libswitch-perl && \
  wget --no-check-certificate -q -O /usr/local/bin/vcfFilter_DP_Freq.pl https://raw.githubusercontent.com/chenlabgccri/CeL-ID/master/vcfFilter_DP_Freq.pl && \
  chmod +x /usr/local/bin/vcfFilter_DP_Freq.pl
  
COPY ccle_long_dp10.fst /opt/ccle_long_dp10.fst

# clean up
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    apt-get autoclean && \
    apt-get autoremove -y && rm -rf /var/lib/{dpkg,cache,log}/ # keep /var/lib/apt

