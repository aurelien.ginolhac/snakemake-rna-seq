# Development version

- Force p-value limits to `c(0, 1)` in cel-ID plot
- Add DEG numbers reporting using different sets of padj/lFC thresholds
- bugfix: URL for hg19 has changed in Ensembl

# v0.5.4 2023-11-07

- Fix warning in plot PCA
- Add final logs of STAR in multiqc
- Update software set to tag 0.8 (improved multiqc)

# v0.5.3 2023-11-02

- Output log, junctions and reads in genes for STAR (#20)
- Screeplot: limit plotting to the first 20 principal components
- Fix warning for pandas 2.1 (#21)

# v0.5.2 2023-10-03

- Restore the tweaked parameters for STAR

# v0.5.1 2023-09-07

### New Features

- Implement tests in CI (fix #14)
- `fastqc` for both pairs in paired-end

### Bugfixes

- Fix pca plot
- Fix mix of single and paired data during `featureCounts`


# v0.5.0 2023-08-22

### New features

- CeL-ID for human cell lines identification available
- Add screeplot for the PCA (fix #17)
- Update sofware sets and snakemake wrappers

### Breaking changes

- Only the blind to design PCA is performed
- Add sample names to the PCA


### Bugfixes

- Be more strict and require strandedness to be filled (fix #15)
- Default adapter sequences shorter to 33 bp as recommended by Illumina (https://bit.ly/3N0x2x4)


# v0.4.0 2022-07-26

- log files from FastQC are not written (fix #13)
- Check that paired-end files are not identical (fix #12)
- Check that config tsv files header are correct (fix #12)
- Check that values in units.tsv columns are unique (fix #11)
- Check that fq2 are not in fq1
- Add helpers to create config tsv files in README
- Add launcher example in README
- New version of software set in Docker image, using r2u (tag 0.6)

# v0.3.2 2021-12-03

- hotfix, typo in mix PE/SE `featureCounts`


# v0.3.1 2021-12-01

- rename accordingly sample names instead of full path for counts (fix #10)

# v0.3.0 2021-11-26

- Add `Dockerfile`
- bugfix in `featureCounts` (#9)
- container directive only specified once (#8)
- check samples in units.tsv and samples.tsv are coherent (fix #4)
- dashes are forbidden in contrasts (fix #5)


## v0.2.3 2020-09-17

- bugfix in `featureCounts` when dealing with paired-end data (fix #3)
- added assertion tests for config files

## v0.2.2 2020-09-10

- bugfix for `AdapterRemoval` paired-end (fix #1)
- units, _i.e_ technical replicates are merged at the mapping step (fix #2)
- clean-up code, linting is now silent
- improve documentation, especially add chapters for writing `units.tsv` and debugging

## v0.2.1 2020-07-13

- replace hard-coded absolute paths after linting by config parameters
- specify `container` for each rule
- move all function definitions to `rules/common.smk`

## v0.2 2020-07-10

- snakemake `5.20.1` release, don't need to stick to outdated version
- use updated singularity image with R v4.0 and Bioconductor 3.11

## v0.1 2020-02-14

- add a changelog
