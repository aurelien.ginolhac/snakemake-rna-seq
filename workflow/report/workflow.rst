This `workflow template <https://gitlab.lcsb.uni.lu/aurelien.ginolhac/snakemake-rna-seq>`  is derived from Johannes Koester `rnaseq-deseq2 <https://github.com/snakemake-workflows/rna-seq-star-deseq2>`_ Snakemake template.
However, reads trimmed by `AdapterRemoval <https://github.com/MikkelSchubert/adapterremoval>`_ and read counting performed by `Rsubread <https://bioconductor.org/packages/release/bioc/html/Rsubread.html>`_.

