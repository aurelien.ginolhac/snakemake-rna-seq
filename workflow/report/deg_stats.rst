Number of differentially expressed genes (up or down-regulated) according to different sets of thresholds
on both adjusted p-value and log2 fold change.
