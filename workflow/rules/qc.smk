
rule multiqc:
    input:
        input_multiqc
    output:
        "qc/multiqc_report.html"
    log:
        "logs/multiqc.log"
    wrapper:
        "v2.4.0/bio/multiqc"

rule fastqc:
    input:
        get_raw_fq
    output:
        html="qc/fastqc/{sample}-{unit}-{read}.html",
        zip="qc/fastqc/{sample}-{unit}-{read}_fastqc.zip"
    params:
        ""
    log:
        "logs/fastqc/{sample}-{unit}-{read}.log"
    wrapper:
        "v2.4.0/bio/fastqc"

rule fastq_screen:
    input:
        get_raw_fq
    output:
        txt="qc/fastq_screen/{sample}-{unit}-{read}_fastq_screen.txt",
        png="qc/fastq_screen/{sample}-{unit}-{read}_fastq_screen.png"
    log:
        "logs/fastq_screen/{sample}-{unit}-{read}.log"
    params:
        fastq_screen_config = {
            'database': {
                'human': {
                  'bowtie2': "{}/Human/Homo_sapiens.GRCh38".format(config["params"]["db_bowtie_path"])},
                'mouse': {
                  'bowtie2': "{}/Mouse/Mus_musculus.GRCm38".format(config["params"]["db_bowtie_path"])},
                'rat':{
                  'bowtie2': "{}/Rat/Rnor_6.0".format(config["params"]["db_bowtie_path"])},
                'drosophila':{
                  'bowtie2': "{}/Drosophila/BDGP6".format(config["params"]["db_bowtie_path"])},
                'worm':{
                  'bowtie2': "{}/Worm/Caenorhabditis_elegans.WBcel235".format(config["params"]["db_bowtie_path"])},
                'yeast':{
                  'bowtie2': "{}/Yeast/Saccharomyces_cerevisiae.R64-1-1".format(config["params"]["db_bowtie_path"])},
                'arabidopsis':{
                  'bowtie2': "{}/Arabidopsis/Arabidopsis_thaliana.TAIR10".format(config["params"]["db_bowtie_path"])},
                'ecoli':{
                  'bowtie2': "{}/E_coli/Ecoli".format(config["params"]["db_bowtie_path"])},
                'rRNA':{
                  'bowtie2': "{}/rRNA/GRCm38_rRNA".format(config["params"]["db_bowtie_path"])},
                'MT':{
                  'bowtie2': "{}/Mitochondria/mitochondria".format(config["params"]["db_bowtie_path"])},
                'PhiX':{
                  'bowtie2': "{}/PhiX/phi_plus_SNPs".format(config["params"]["db_bowtie_path"])},
                'Lambda':{
                  'bowtie2': "{}/Lambda/Lambda".format(config["params"]["db_bowtie_path"])},
                'vectors':{
                  'bowtie2': "{}/Vectors/Vectors".format(config["params"]["db_bowtie_path"])},
                'adapters':{
                  'bowtie2': "{}/Adapters/Contaminants".format(config["params"]["db_bowtie_path"])},
                'mycoplasma':{
                  'bowtie2': "{}/Mycoplasma/mycoplasma".format(config["params"]["db_bowtie_path"])}
                 },
                 'aligner_paths': {'bowtie2': "{}/bowtie2".format(config["params"]["bowtie_path"])}
                },
        subset=100000,
        aligner='bowtie2'
    threads: 8
    wrapper:
        "v2.4.0/bio/fastq_screen"

rule session:
    output:
        report("results/session.txt", category = "Versions", caption = "../report/session.rst")
    log:
        "logs/session.log"
    script:
        "../scripts/session.R"

