
# CeL-ID workflow
# publication: https://bmcgenomics.biomedcentral.com/track/pdf/10.1186/s12864-018-5371-9.pdf
# methodology: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7643850/pdf/BioProtoc-10-03-3507.pdf
# GitHub: https://github.com/chenlabgccri/CeL-ID

rule mpileup:
    input:
        # single or list of bam files
        bam="dedup_bam/{sample}.bam",
        bai="dedup_bam/{sample}.bai",
        ref="refs/{}.fasta".format(config["ref"]["build"]),
    output:
        temp("mpileup/{sample}.mpileup.gz"),
    log:
        "logs/samtools/mpileup/{sample}.log",
    params:
        extra="-d 10000 -q 10 -C 50 -B ",  # options from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7643850/pdf/BioProtoc-10-03-3507.pdf
    shell:
        "samtools mpileup {params.extra} -f {input.ref} {input.bam} | gzip > {output} 2> {log}"

rule mpileup_to_vcf:
    input:
        "mpileup/{sample}.mpileup.gz"
    output:
        "vcf/{sample}.vcf"
    message:
        "Calling SNP with Varscan2"
    threads:  # Varscan does not take any threading information
        2     # However, mpileup might have to be unzipped.
              # Keep threading value to one for unzipped mpileup input
              # Set it to two for zipped mipileup files
    # optional specification of memory usage of the JVM that snakemake will respect with global
    # resource restrictions (https://snakemake.readthedocs.io/en/latest/snakefiles/rules.html#resources)
    # and which can be used to request RAM during cluster job submission as `{resources.mem_mb}`:
    # https://snakemake.readthedocs.io/en/latest/executing/cluster.html#job-properties
    resources:
        mem_mb=4096
    log:
        "logs/varscan_{sample}.log"
    params:
      extra=" --min-avg-qual 10 --mpileup 1 --min-coverage 10 --min-var-freq 0.05 --output-vcf 1 --p-value 0.05 --variants 1"
    shell:
      "java -jar /opt/varscan.jar mpileup2cns <(zcat {input} | sed 's/^chr//')  {params.extra} > {output} 2> {log}"


# since we use hg38, we need now to liftOver to hg19 for the original CeL-ID paper
# 1) we need to chain for conversion
# http://hgdownload.soe.ucsc.edu/goldenPath/hg38/liftOver/hg38ToHg19.over.chain.gz
# Need to remove the chr prefix as we won't have 
rule dw_genome_chain:
    output: "refs/hg38ToHg19.over.chain"
    threads:  1
    log: "logs/dw_lift_over_chain.log"
    params: "http://hgdownload.soe.ucsc.edu/goldenPath/hg38/liftOver/hg38ToHg19.over.chain.gz"
    shell:
      """
      wget -q -O - {params} | gunzip -c | sed -e 's/chr//g' > {output}
      """
# -e 's/v1/.1/g' -e 's/Un_//'

# 2) the hg19 genome (target) for liftOver -R option
rule get_genome_hg19:
    output:
        temp("refs/GRCh37.fasta")
    params: "https://ftp.ensembl.org/pub/grch37/release-{}/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.dna_sm.primary_assembly.fa.gz".format(config["ref"]["release"])
    log:
        "logs/genome_GRCh37.log"
    shell:
      """
      wget -q -O - {params} | gunzip -c > {output}
      """


rule create_seq_dict:
    input: "refs/GRCh37.fasta"
    output: temp("refs/GRCh37.dict")
    threads:  1
    log: "logs/create_seq_dict.log"
    shell:
      """
      picard CreateSequenceDictionary R={input} O={output} 2> {log}
      """

rule liftOver_vcf:
    input:
        vcf="vcf/{sample}.vcf",
        chain="refs/hg38ToHg19.over.chain",
        seqdict="refs/GRCh37.dict",
        ref="refs/GRCh37.fasta"
    output:
        vcf="vcf_hg19/{sample}.vcf",
        reject="vcf_hg19/{sample}_rejected.vcf"
    message:
        "LiftOver SNP to hg19"
    threads:  12
    resources:
        mem_gb=48
    log:
        "logs/lift_over_vcf_{sample}.log"
    params: ""
    shell:
      # options added from https://www.biostars.org/p/9549529/#9549773, no swap alleles: crash vcfFilter_DP_Freq.pl
      # WMC is WARN ON MISSING CONTIG, too bad if we miss some on unplaced contigs
      """
      java -Xmx48G -jar /opt/picard.jar LiftoverVcf I={input.vcf} O={output.vcf} WMC=true MAX_RECORDS_IN_RAM=50000 CHAIN={input.chain} REJECT={output.reject} R={input.ref} 2> {log}
      """

# http://hgdownload.soe.ucsc.edu/goldenPath/hg38/liftOver/hg38ToHg19.over.chain.gz

rule filter_vcf:
    input:
        "vcf_hg19/{sample}.vcf"
    output:
        "vcf_filtered/{sample}_final.vcf",
        "vcf_filtered/{sample}_FREQ.txt",
        "vcf_filtered/{sample}_DP.txt"
    message:
        "Filter SNP with CeL-ID"
    threads:  1
    resources:
        mem_mb=4096
    log:
        "logs/vcf_filter_{sample}.log"
    params: " --DP=30 --AD=5 --FREQ=25" # https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7643850/pdf/BioProtoc-10-03-3507.pdf
    shell:
      """
      mkdir -p vcf_filtered
      echo {wildcards.sample} > vcf_filtered/{wildcards.sample}_samplelist
      vcfFilter_DP_Freq.pl --input={input} --list=vcf_filtered/{wildcards.sample}_samplelist {params} 2> {log}
      mv vcf_hg19/{wildcards.sample}_final.vcf vcf_hg19/{wildcards.sample}_DP.txt vcf_hg19/{wildcards.sample}_FREQ.txt vcf_filtered
      rm -f vcf_filtered/{wildcards.sample}_samplelist
      """

rule celid_inference:
    input:
        FREQ="vcf_filtered/{sample}_FREQ.txt",
        DP="vcf_filtered/{sample}_DP.txt"
    output:
        tsv="results/cel-id_{sample}.tsv"
    message:
        "Running CeL-ID inference on filtered variants"
    params:
        ""
    log:
        "logs/cel-id_inference_{sample}.log"
    script:
        "../scripts/cel-id_inference.R"

rule celid_plot:
    input:
        #"results/cel-id_T18_control_4h_2019.tsv"
        expand("results/cel-id_{unit.sample}.tsv", unit=units.itertuples())
    output:
        "results/cel-id_plot.pdf"
    message:
        "Aggregating CeL-ID results"
    params:
        ""
    log:
        "logs/cel-id_plot.log"
    script:
        "../scripts/cel-id_plot.R"
