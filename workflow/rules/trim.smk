
# adapted from https://github.com/UofABioinformaticsHub/RNAseq_snakemake/blob/master/snakefile.py
rule adapter_removal_pe:
    input:
        # unpack to convert the returned dict to a list
        unpack(get_fastq)
    output:
        fq1="trimmed_pe/{sample}-{unit}.1.fastq.gz",
        fq2="trimmed_pe/{sample}-{unit}.2.fastq.gz",
        single="trimmed_pe/{sample}-{unit}.singletons.gz",
        discarded="trimmed_pe/{sample}-{unit}.discarded.gz",
        settings="trimmed_pe/{sample}-{unit}.settings"
    params:
        min_len = config["trimming"]["min_length"],
        adapter1 = config["trimming"]["adapter1"],
        adapter2 = config["trimming"]["adapter2"]
    threads: config["trimming"]["threads"]
    log:
        "logs/adapterremoval/{sample}-{unit}.log"
    shell:
        "AdapterRemoval --file1 {input.fq1} --file2 {input.fq2} "
        "--output1 {output.fq1} --output2 {output.fq2} "
        "--singleton {output.single} "
        "--settings {output.settings} --discarded {output.discarded} "
        "--adapter1 {params.adapter1} --adapter2 {params.adapter2} "
        "--threads {threads} --gzip --trimqualities --trimns "
        "--minlength {params.min_len}"
# adapted from https://github.com/UofABioinformaticsHub/RNAseq_snakemake/blob/master/snakefile.py
rule adapter_removal_se:
    input:
      unpack(get_fastq)
    output:
        fq1="trimmed_se/{sample}-{unit}.fastq.gz",
        settings="trimmed_se/{sample}-{unit}.settings",
        discarded="trimmed_se/{sample}-{unit}.discarded.gz"
    params:
        min_len = config["trimming"]["min_length"],
        adapter1 = config["trimming"]["adapter1"],
    threads: config["trimming"]["threads"]
    log:
        "logs/adapterremoval/{sample}-{unit}.log"
    shell:
        "AdapterRemoval --file1 {input.fq1} "
        "--output1 {output.fq1} "
        "--settings {output.settings} "
        "--discarded {output.discarded} "
        "--adapter1 {params.adapter1} "
        "--threads {threads} "
        "--gzip --trimqualities --trimns "
        "--minlength {params.min_len}"
