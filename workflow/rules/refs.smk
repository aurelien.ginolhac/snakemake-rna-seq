build = config["ref"]["build"]

rule get_genome:
    output:
        "refs/{build}.fasta"
    params:
        species=config["ref"]["species"],
        datatype="dna",
        build=config["ref"]["build"],
        release=config["ref"]["release"]
    log:
        "logs/genome_{build}.log"
    wrapper:
        "v2.6.0/bio/reference/ensembl-sequence"

rule get_annotation:
    output:
        "refs/{build}.gtf"
    params:
        species=config["ref"]["species"],
        build=config["ref"]["build"],
        release=config["ref"]["release"],
        fmt="gtf"
    log:
        "logs/annotation_{build}.log"
    wrapper:
        "v2.6.0/bio/reference/ensembl-annotation"
