rule star_index:
    input:
        fasta = "refs/{}.fasta".format(config["ref"]["build"]),
        gtf = "refs/{}.gtf".format(config["ref"]["build"])
    output:
        directory("refs/{}".format(config["ref"]["build"]))
    message:
        "STAR indexing {}".format(config["ref"]["build"])
    threads:
        12
    params:
        extra = ""
    log:
        "logs/star_index_{}.log".format(config["ref"]["build"])
    wrapper:
        "v2.3.2/bio/star/index"

rule align_multi:
    input:
        # this function is called as many samples are provided in output
        # technical replicates are merged in STAR
        fq1 = get_fq1,
        fq2 = get_fq2, # optional, return none if not in units.tsv
        idx = rules.star_index.output
    output:
        aln="star/{sample}/Aligned.sortedByCoord.out.bam",
        log="logs/star/{sample}/Log.out",
        log_final="logs/star/{sample}/Log.final.out",
        sj="star/{sample}/SJ.out.tab",
        reads_per_gene= "star/{sample}/ReadsPerGene.out.tab"
    log:
        "logs/star/{sample}.log"
    params:
        # path to STAR reference build index
        build = config["ref"]["build"],
        index = rules.star_index.output,
        # optional parameters
        extra = "--quantMode GeneCounts {}".format(config["params"]["star"])
    threads: 12
    wrapper:
        "v2.6.0/bio/star/align"


rule markduplicates_bam:
    input:
        bams="star/{sample}/Aligned.sortedByCoord.out.bam",
    # optional to specify a list of BAMs; this has the same effect
    # of marking duplicates on separate read groups for a sample
    # and then merging
    output:
        bam="dedup_bam/{sample}.bam",
        bai="dedup_bam/{sample}.bai",
        metrics="dedup_bam/{sample}.metrics.txt"
    log:
        "logs/dedup_bam/{sample}.log"
    resources:
        mem_mb=4096,
    threads: 12 # to avoid running too many in parallel
    params:
        "SORTING_COLLECTION_SIZE_RATIO=0.12 REMOVE_DUPLICATES=true CREATE_INDEX=true ASSUME_SORTED=true VALIDATION_STRINGENCY=LENIENT"
    wrapper:
        "0.64.0/bio/picard/markduplicates"

